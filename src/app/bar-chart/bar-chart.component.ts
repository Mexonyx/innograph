import { Component, OnInit } from '@angular/core';
import * as echarts from "echarts";

@Component({
  selector: "app-bar-chart",
  templateUrl: "./bar-chart.component.html",
  styleUrls: ["./bar-chart.component.css"]
})
export class BarChartComponent implements OnInit {
  heureDetenu: number;
  heureDetenuRemain: number;
  heureDetenuConst: number;
  heureDetenuRemainConst: number;
  pieChart: any;
  pieChart2: any;
  ngOnInit() {
    this.heureDetenu = 0;
    this.heureDetenuRemain = 20;
    this.heureDetenuConst = 5;
    this.heureDetenuRemainConst = 19;
    const graphElement = document.getElementById('graph');
    const graphElement2 = document.getElementById('graph2');
    if (graphElement) {
      this.pieChart = echarts.init(graphElement);
      this.pieChart.setOption({
        graphic:{
          type: 'text',
          left: 'center',
          top: 'center',
          style: {
          text: '{hours|'+(this.heureDetenu+this.heureDetenuRemain)+'}'+'\n{totalText|Total}',
          textAlign: 'center',
          fill: '#494F57',
          font: 'bold 32px Marianne',
          rich:{
            hours: {
              fontFamily: 'Marianne',
              fontSize: 32,
              fontStyle: 'normal',
              fontWeight: 700,
            },
            totalText: {
              color: '#494F57',
              align: 'center',
              fontFamily: 'Marianne',
              fontSize: 14,
              fontStyle: 'normal',
              fontWeight: 400,
            }
          }
        }},
        title: {
          text: 'Nb D\'heures individuelles',
          left: 'center'
        },
        tooltip: {
        },
        legend: {
          show: true,
          bottom: 10,
          itemWidth: 12,
          itemHeight: 12,
          left: 'center',
          orient: 'vertical',
          formatter: name => {
            var series = this.pieChart.getOption().series[0];
            var value = series.data.filter(row => row.name === name)[0].value;
            return name + '             '+ value+'h';
          }
        },
        series: [
          {
            max: 20,
            radius: ['55%', '70%'],
            type: 'pie',
            data: [{
                name: 'Validées',
                value: this.heureDetenu,
                label: {
                  show: false,
                },
                itemStyle: {
                  color: '#574098'
                }
              },
              {
                name: 'Restantes',
                value: this.heureDetenuRemain,
                itemStyle: {
                  color: 'rgba(0, 0, 0, 0.2)',
                },
                label: {
                  show: false,
                },
                tooltip: {
                  show: false
               }
              }
            ],
            animationDuration: 3000,
          }
        ]
      });
      
    } else{
      console.log("non");
    }
    if(graphElement2){
      this.pieChart2 = echarts.init(graphElement2);
      this.pieChart2.setOption({
        title: {
          text: 'Nb D\'heures collectives',
          left: 'center'
        },
        graphic:{
          type: 'text',
          left: 'center',
          top: 'center',
          style: {
          text: '{hours|'+(this.heureDetenuConst+this.heureDetenuRemainConst)+'}'+'\n{totalText|Total}',            textAlign: 'center',
          fill: '#494F57',
          font: 'bold 32px Marianne',
          rich:{
            hours: {
              fontFamily: 'Marianne',
              fontSize: 32,
              fontStyle: 'normal',
              fontWeight: 700,
            },
            totalText: {
              color: '#494F57',
              align: 'center',
              fontFamily: 'Marianne',
              fontSize: 14,
              fontStyle: 'normal',
              fontWeight: 400,
            }
          }
        }},
        legend: {
          show: true,
          bottom: 10,
          itemWidth: 12,
          itemHeight: 12,
          left: 'center',
          orient: 'vertical',
          formatter: name => {
            var series = this.pieChart2.getOption().series[0];
            var value = series.data.filter(row => row.name === name)[0].value;
            return name + '             '+ value+'h';
          }
        },
        series: [
          {
            max: 24,
            radius: ['55%', '70%'],
            type: 'pie',
            data: [{
              name: 'Validées',
              value: this.heureDetenuConst,
              label: {
                show:false,
              },
              itemStyle: {
                color: '#574098'
              }
              },
              {
                name: 'Restantes',
                value: this.heureDetenuRemainConst,
                itemStyle: {
                  color: 'rgba(0, 0, 0, 0.2)',
                },
                label: {
                  show: false,
                },
                tooltip: {
                  show: false
               }
              }
            ],
            animationDuration: 3000,
          }
        ]
      });
    }
  }

  public buttonClicked(){
    this.heureDetenu +=1;
    this.heureDetenuRemain -=1;
    this.pieChart.setOption({
      series:[
        {
        data: [{
          name: 'Validées',
          value: this.heureDetenu,
          label: {
            show: false,
          },
          itemStyle: {
            color: '#574098'
          }
        },
        {
          name: 'Restantes',
          value: this.heureDetenuRemain,
          itemStyle: {
            color: 'rgba(0, 0, 0, 0.2)',
          },
          label: {
            show: false,
          },
          tooltip: {
            show: false
         }
        }]
       }
      ]
    })
    console.log(this.heureDetenu)
  }
}
